﻿using חסידות.General;

namespace חסידות.מעלהומטה
{
    public interface Iמלמטהלמעלה : ICauseAndEffect<Iמטה, Iמעלה>
    {
        Iמטה ICauseAndEffect<Iמטה, Iמעלה>.Cause => null;
        Iמעלה ICauseAndEffect<Iמטה, Iמעלה>.Effect => null;
    }
}