﻿using חסידות.General;

namespace חסידות.מעלהומטה
{
    public interface Iמלמעלהלמטה : ICauseAndEffect<Iמעלה, Iמטה>
    {
        Iמעלה ICauseAndEffect<Iמעלה, Iמטה>.Cause => null;
        Iמטה ICauseAndEffect<Iמעלה, Iמטה>.Effect => null;
    }
}