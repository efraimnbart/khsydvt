﻿namespace חסידות.מעלהומטה
{
    public interface Iמעלהומטה<out Tמלמעלהלמטה, out Tמלמטהלמעלה> 
        where Tמלמעלהלמטה : Iמלמעלהלמטה 
        where Tמלמטהלמעלה : Iמלמטהלמעלה
    {
        Tמלמעלהלמטה מלמעלהלמטה { get; }
        Tמלמטהלמעלה מלמטהלמעלה { get; }
    }
}