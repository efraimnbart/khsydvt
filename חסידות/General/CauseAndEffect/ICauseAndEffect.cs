﻿namespace חסידות.General
{
    public interface ICauseAndEffect<out TCause, out TEffect>
        where TCause : ICause
        where TEffect : IEffect
    {
        TCause Cause { get; }
        TEffect Effect { get; }
    }
}