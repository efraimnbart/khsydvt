﻿using חסידות.מעלהומטה;

namespace חסידות.טלומטר
{
    public class טלומטר : Iמעלהומטה<טל, מטר>
    {
        public טל מלמעלהלמטה { get; }
        public מטר מלמטהלמעלה { get; }
    }
}